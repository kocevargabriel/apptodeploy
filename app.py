#from app import app
#from app import server
from datetime import datetime as dt
import dash
import dash_html_components as html
import dash_core_components as dcc
import dash_design_kit as ddk
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output
import dash_daq as daq
from datetime import datetime
import dash_table
import pandas as pd
import numpy as np
import plotly.express as px
import plotly.graph_objs as go
from Appfuns import segDTW, allDTW


external_stylesheets = ["https://codepen.io/chriddyp/pen/bWLwgP.css"]
app = dash.Dash(__name__) 
server = app.server
app.config.suppress_callback_exceptions = True


Data_Foulon1 = pd.read_csv("./assets/data_curve.csv")
Foulon1 = pd.read_csv("./assets/LotF1.csv", sep=";")
cc = pd.read_csv("./assets/cc.csv")
IC_durrees = pd.read_csv("./assets/IC_Durée.csv")
col_dechaulage = '#b7e1f8'
col_pickle = 'LightSalmon'
col_fin_pickle = '#ffff96'
col_pretannage = "#ffc9a3"
col_tannage = '#a3ffe6'
styles = {
    'pre': {
        'border': 'thin lightgrey solid',
        'overflowX': 'scroll'
    }
}

tabs_styles = {
    'height': '50px'
}
tab_style = {
    'borderBottom': '0px solid #d6d6d6',
    'padding': '6px'
}

tab_selected_style = {
    'borderTop': '0px solid #d6d6d6',
    'borderBottom': '1px solid #d6d6d6',
    'backgroundColor': '#119DFF',
    'color': 'white',
    'padding': '6px',
    'fontWeight': 'bold'
}


app.layout = ddk.App([
        ddk.Header(style={'height' : '10vh', 'background' : '#1A77BD', "marginBottom" : "0px"}, children = [
        ddk.Logo(app.get_asset_url("logo.png"),style={'position' : 'right'}),
        ddk.Title('Tannerie 4.0 - Capteurs Foulons', style={'align' : 'center', 'color' : 'white'})
    ]),

    
# ---------------- #
# --- Onglet 1 --- #
# ---------------- #
    dcc.Tabs(style = tabs_styles, children = [
        dcc.Tab(label='Temps Réel', style=tab_style, selected_style=tab_selected_style, children=[
            ddk.Sidebar(
                style = {'width' : '20vw', 'background' : '#002060' , 'height' : '85vh'},
                children=[
                    ddk.Title("Sidebar", style = {'color' : 'white'}),
                    ddk.ControlCard(
                        type='flat',
                        children=[
                            ddk.ControlItem(
                                dcc.Dropdown(
                                    id='Foulon',
                                    options=[],
                                    value=["Foulon 1"]
                                ),
                            ),
                        ]
                    )    
                ]
            ),

            ddk.SidebarCompanion([
                html.Div([
                    html.Div([
                        html.H3('Column 1'),
                        dcc.Graph(id='g1', figure={'data': [{'y': [1, 2, 3]}]})
                    ], className="nine columns"),

                    html.Div([
                        html.H3('Column 2'),
                        dcc.Graph(id='g2', figure={'data': [{'y': [1, 2, 3]}]})
                    ], className="three columns"),
                ], className="row")
                #dcc.Graph(id='Scatter_plot')
            ])

        ]),

# ---------------- #
# --- Onglet 2 --- #
# ---------------- #

        dcc.Tab(label='Analyse d\'un lot', style=tab_style, selected_style=tab_selected_style, children=[
            ddk.Sidebar(
                style = {'width' : '20vw', 'background' : '#002060' , 'height' : '85vh'},
                children=[
                    ddk.ControlCard(
                        type='flat',
                        style={'background' : '#002060'},
                        children=[
                            html.Label(["Période"], style = {'color' : 'white', "display": "block", "text-align": "left"}),
                            dcc.DatePickerRange(
                                id='my-date-picker-range',
                                display_format="DD/MM/YY",
                                min_date_allowed=dt(1995, 8, 5),
                                max_date_allowed=dt(2017, 9, 19),
                                initial_visible_month=dt(2017, 8, 5),
                                with_portal=True,
                                end_date=dt(2017, 8, 25).date(), 
                                style = {"font-size": 14}
                            ),
                            html.Br(),
                            html.Br(),
                            html.Label(["Formule"], style = {'color' : 'white', "display": "block", "text-align": "left"}),
                            dcc.Dropdown(
                                id='Formule',
                                options=[{'label': i, 'value': i} for i in Foulon1.Formule.unique()],
                                value=Foulon1.Formule.unique()[0], 
                                clearable = False
                            ),
                            html.Br(),
                            html.Label(["Lot"], style = {'color' : 'white', "display": "block", "text-align": "left"}),
                            dcc.Dropdown(
                                    id='Lot',
                                    options=[{'label': i, 'value': i} for i in Data_Foulon1.NomLot.unique()],
                                    value=Data_Foulon1.NomLot.unique()[0],
                                    clearable = False
                                ),
                                html.Br(),
                                dcc.Checklist(
                                    id = "RawData_Tab2",
                                    options=[
                                        {'label': 'Données brutes', 'value': 'Raw'},
                                    ],
                                    value=[],
                                    labelStyle = {'color' : 'white'}
                                ),
                                html.Br(),
                                html.Br(),
                                html.Br(),
                                html.Br(),
                                html.Br(),
                                html.Br(),
                                html.Br()
                        ]
                    ),
                    
                ]
            ),

            ddk.SidebarCompanion(children = [
                html.Div([
                    html.Div([
                        dcc.Graph(id = 'Scatter_plot2',
                        config={
                            "displaylogo": False,
                            'modeBarButtonsToRemove': ['lasso2d', 'hoverClosestCartesian', 'hoverCompareCartesian', 'toggleSpikelines', 'resetScale2d']
                        })
                    ], className="eight columns"),

                    html.Div([
                        html.Div([
                        ], className = "row"),
                        html.Div([
                            html.Div([
                                daq.Gauge(style={'margin-bottom': -70},
                                    id = 'gauge_o2_dechaulage',
                                    color=col_dechaulage,
                                    showCurrentValue=True,
                                    units="Heures",
                                    labelPosition="bottom",
                                    size=160,
                                    value=0, label='Déchaulage', max=5, min=0
                                )
                            ],className="six columns"),
                            html.Div([  
                                daq.Gauge(style={'margin-bottom': -70},
                                    id = 'gauge_o2_pickle',
                                    color=col_pickle,
                                    units="Heures",
                                    labelPosition="bottom",
                                    size=160,
                                    showCurrentValue=True,
                                    value=0, label='Pickle', max=5, min=0)
                            ], className="six columns")
                        ], className = "row"),
                        html.Div([
                            html.Div([
                                daq.Gauge(style={'margin-bottom': -70},
                                    id = 'gauge_o2_fin_pickle',
                                    color=col_fin_pickle,
                                    showCurrentValue=True,
                                    units="Heures",
                                    labelPosition="bottom",
                                    size=160,
                                    value=0, label='Fin de Pickle', max=5, min=0
                                )
                            ],className="six columns"),
                            html.Div([  
                                daq.Gauge(style={'margin-bottom': -70},
                                    id = 'gauge_o2_pretannage',
                                    color=col_pretannage,
                                    showCurrentValue=True,
                                    units="Heures",
                                    labelPosition="bottom",
                                    size=160,
                                    value=0, label='Préntannage - Tannage', max=5, min=0)
                            ], className="six columns")
                        ], className = "row")
                    ], className="four columns"),

                ], className="row"),
                html.Div([
                    html.Div([
                        dcc.Graph(id = 'Scatter_plot3',
                        config={
                            "displaylogo": False,
                        'modeBarButtonsToRemove': ['lasso2d', 'hoverClosestCartesian', 'hoverCompareCartesian', 'toggleSpikelines', 'resetScale2d']
                        })
                    ], className="eight columns"),

                    html.Div([
                        html.Div([
                            html.Label(["Détection des périodes"], 
                            style = {'color' : 'black', "display": "block", "text-align": "left", "marginBottom" : "10px", 'fontWeight': 'bold'})
                        ], className = "row"),
                        html.Div([
                            html.Div([
                                daq.Indicator(
                                    label="Déchaulage",
                                    labelPosition="bottom",
                                    color="#84e61c",
                                    value=True
                                )  
                            ],className="three columns"),
                            html.Div([
                                daq.Indicator(
                                    label="Pickle",
                                    labelPosition="bottom",
                                    color="#84e61c",
                                    value=True
                                )  
                            ],className="three columns"),
                            html.Div([
                                daq.Indicator(
                                    label="Fin de pickle",
                                    labelPosition="bottom",
                                    color="#84e61c",
                                    value=True
                                )  
                            ],className="three columns"),
                            html.Div([
                                daq.Indicator(
                                    label="Prétannage",
                                    labelPosition="bottom",
                                    color="#84e61c",
                                    value=True
                                )  
                            ],className="three columns"),
                        ], className = "row"),  
                        html.Div([
                            html.Label(["Durée des périodes"], 
                            style = {'color' : 'black', "display": "block", "text-align": "left", "marginBottom" : "10px", "marginTop" : "20px", 'fontWeight': 'bold'})
                        ], className = "row"),
                        html.Div([
                            html.Div([
                                daq.Indicator(
                                    id = "o2_Time_Dechaulage_ind",
                                    label=" ",
                                    labelPosition="left",
                                    color="#84e61c",
                                    value=True
                                )  
                            ],className="one columns"),
                            html.Div([
                                html.P(id = "o2_Time_Dechaulage", children = ["init"],
                                style = {'color' : 'black', "display": "block", "text-align": "left", "marginTop" : "-5px"})
                            ],className="eleven columns")
                        ], className = "row"),
                        html.Div([
                            html.Div([
                                daq.Indicator(
                                    id = "o2_Time_Pickle_ind",
                                    label=" ",
                                    labelPosition="left",
                                    color="#84e61c",
                                    value=True
                                )  
                            ],className="one columns"),
                            html.Div([
                                html.P(id = "o2_Time_Pickle", children = ["init"],
                                style = {'color' : 'black', "display": "block", "text-align": "left", "marginTop" : "-5px"})
                            ],className="eleven columns")
                        ], className = "row"),   
                        html.Div([
                            html.Div([
                                daq.Indicator(
                                    id = "o2_Time_FinPickle_ind",
                                    label=" ",
                                    labelPosition="left",
                                    color="#84e61c",
                                    value=True
                                )  
                            ],className="one columns"),
                            html.Div([
                                html.P(id = "o2_Time_FinPickle", children = ["init"],
                                style = {'color' : 'black', "display": "block", "text-align": "left", "marginTop" : "-5px"})
                            ],className="eleven columns")
                        ], className = "row"),  
                        html.Div([
                            html.Div([
                                daq.Indicator(
                                    id = "o2_Time_Pretannage_ind",
                                    label=" ",
                                    labelPosition="left",
                                    color="#84e61c",
                                    value=True
                                )  
                            ],className="one columns"),
                            html.Div([
                                html.P(id = "o2_Time_Pretannage", children = ["init"],
                                style = {'color' : 'black', "display": "block", "text-align": "left", "marginTop" : "-5px"})
                            ],className="eleven columns")
                        ], className = "row"),                  
                    ], className="four columns"),
                ], className="row")
            ])
        ]),

# ---------------- #
# --- Onglet 3 --- #
# ---------------- #

        dcc.Tab(label='Comparaison de deux lots', style=tab_style, selected_style=tab_selected_style, children=[
            ddk.Sidebar(
                style = {'width' : '20vw', 'background' : '#002060' , 'height' : '85vh'},
                children=[
                    ddk.Title("Lot de référence", style = {'color' : 'white'}),
                    ddk.ControlCard(
                        type='flat',
                        style={'background' : '#002060'},
                        children=[
                            html.Label(["Période"], style = {'color' : 'white', "display": "block", "text-align": "left"}),
                            dcc.DatePickerRange(
                                id='date_o2_LotRef',
                                display_format="DD/MM/YY",
                                min_date_allowed=dt(1995, 8, 5),
                                max_date_allowed=dt(2017, 9, 19),
                                initial_visible_month=dt(2017, 8, 5),
                                with_portal=True,
                                end_date=dt(2017, 8, 25).date(), 
                                style = {"font-size": 14}
                            ),
                            html.Br(),
                            html.Label(["Formule"], style = {'color' : 'white', "display": "block", "text-align": "left"}),
                            dcc.Dropdown(
                                id='formule_o2_LotRef',
                                options=[{'label': i, 'value': i} for i in Foulon1.Formule.unique()],
                                value=Foulon1.Formule.unique()[0], 
                                clearable = False
                            ),
                            html.Br(),
                            html.Label(["Lot :"], style = {'color' : 'white', "display": "block", "text-align": "left"}),
                            ddk.ControlItem(
                                dcc.Dropdown(
                                    id='LotRef',
                                    options=[{'label': i, 'value': i} for i in Data_Foulon1.NomLot.unique()],
                                    value=Data_Foulon1.NomLot.unique()[0],
                                    clearable = False
                                ),
                            ),
                        ]
                    ),
                    ddk.Title("Lot analysé", style = {'color' : 'white'}),
                    ddk.ControlCard(
                        type='flat',
                        style={'background' : '#002060'},
                        children=[
                            html.Label(["Période"], style = {'color' : 'white', "display": "block", "text-align": "left"}),
                            dcc.DatePickerRange(
                                id='date_o2_LotTest',
                                display_format="DD/MM/YY",
                                min_date_allowed=dt(1995, 8, 5),
                                max_date_allowed=dt(2017, 9, 19),
                                initial_visible_month=dt(2017, 8, 5),
                                with_portal=True,
                                end_date=dt(2017, 8, 25).date(),
                                style = {"font-size": 14}
                            ),
                            html.Br(),
                            html.Label(["Formule"], style = {'color' : 'white', "display": "block", "text-align": "left"}),
                            dcc.Dropdown(
                                id='formule_o2_LotTest',
                                options=[{'label': i, 'value': i} for i in Foulon1.Formule.unique()],
                                value=Foulon1.Formule.unique()[0], 
                                clearable = False
                            ),
                            html.Br(),
                            html.Label(["Lot :"], style = {'color' : 'white', "display": "block", "text-align": "left"}),
                            ddk.ControlItem(
                                dcc.Dropdown(
                                    id='LotTest',
                                    options=[{'label': i, 'value': i} for i in Data_Foulon1.NomLot.unique()],
                                    value=Data_Foulon1.NomLot.unique()[1],
                                    clearable = False
                                ),
                            ),
                            html.Br(),
                            html.Br(),
                            html.Br()
                        ]
                    ),
                ]
            ),
            ddk.SidebarCompanion(children = [
                
                html.Div(children = [
                    dcc.Graph(id = 'Scatter_plot6',
                config={
                    "displaylogo": False,
                    'modeBarButtonsToRemove': ['lasso2d', 'hoverClosestCartesian', 'hoverCompareCartesian', 'toggleSpikelines', 'resetScale2d']
                })],
                style = {'display': 'inline-block', 'width': '100%'}),
                html.Div(children = [
                    dcc.Graph(id = 'Scatter_plot7',
                config={
                    "displaylogo": False,
                    'modeBarButtonsToRemove': ['lasso2d', 'hoverClosestCartesian', 'hoverCompareCartesian', 'toggleSpikelines', 'resetScale2d']
                })],
                style = {'display': 'inline-block', 'width': '100%'}),
                    ],
                ),
        ]),

# ---------------- #
# --- Onglet 4 --- #
# ---------------- #

        dcc.Tab(label='Base des alertes', style=tab_style, selected_style=tab_selected_style, children=[
            ddk.Sidebar(
                style = {'width' : '20vw', 'background' : '#002060' , 'height' : '85vh'},
                children=[
                    ddk.Title("Options", style = {'color' : 'white'}),
                    ddk.ControlCard(
                        type='flat',
                        style={'background' : '#002060'},
                        children=[
                            ddk.ControlItem(
                                dcc.Dropdown(
                                    id='LotO4',
                                    options=[{'label': i, 'value': i} for i in Foulon1["NomLot"]],
                                    value=Foulon1["NomLot"][0]
                                ))])]),

            ddk.SidebarCompanion([
                dash_table.DataTable(
                    columns=[{"name" : i, "id" : i} for i in Foulon1.columns],
                    data=Foulon1.to_dict('records'),
                    style_table={
                        'maxHeight' : '76vh',
                        'overflowY': 'scroll'
                    })
                    ])
        ])
    ])
])

@app.callback(
    [Output('gauge_o2_dechaulage', 'value'),
    Output('gauge_o2_dechaulage', 'color'),
    Output('gauge_o2_dechaulage', 'min'),
    Output('gauge_o2_dechaulage', 'max'),
    Output('o2_Time_Dechaulage', 'children'),
    Output('o2_Time_Dechaulage_ind', 'color')],
    [Input('Lot', 'value'),
     Input("RawData_Tab2", "value")]
)
def update_gauge_o2_dechaulage(lot_name, raw):
    data = Data_Foulon1[Data_Foulon1["NomLot"]==lot_name]
    duree = (data[data.Period == 'Dechaulage'].Temps.max() - data[data.Period == 'Dechaulage'].Temps.min())/3600
    Low = IC_durrees["Dechaulage"][2]/3600
    Up = IC_durrees["Dechaulage"][3]/3600
    Lower = IC_durrees["Dechaulage"][0]/3600
    Upper = IC_durrees["Dechaulage"][1]/3600
    min = 0
    max = round(1.2*Upper, 1)
    color={"gradient":False,"ranges":{"rgba(228,56,35,1)":[min,Low],"rgba(242, 144, 24,1)": [Low,Lower], col_dechaulage:[Lower,Upper],"#f29018": [Upper,Up],"red":[Up,max]}}
    if duree < Low:
        text = "Déchaulage : Trop court"
        col = "rgba(228,56,35,1)"
    elif duree < Lower:
        text = "Déchaulage : Un peu trop court"
        col = "rgba(242, 144, 24,1)"
    elif duree > Up:
        text = "Déchaulage : Trop long"
        col = "rgba(228,56,35,1)"
    elif duree > Upper: 
        text = "Déchaulage : Un peu trop long"
        col = "rgba(242, 144, 24,1)"
    else :
        text = "Déchaulage : Dans l'intervalle de tollérance"
        col = "#84e61c"
    return [duree, color, min, max, text,col]


@app.callback(
    [Output('gauge_o2_pickle', 'value'),
    Output('gauge_o2_pickle', 'color'),
    Output('gauge_o2_pickle', 'min'),
    Output('gauge_o2_pickle', 'max'),
    Output('o2_Time_Pickle', 'children'),
    Output('o2_Time_Pickle_ind', 'color')],
    [Input('Lot', 'value'),
     Input("RawData_Tab2", "value")]
)
def update_gauge_o2_pickle(lot_name, raw):
    data = Data_Foulon1[Data_Foulon1["NomLot"]==lot_name]
    duree = (data[data.Period == 'Pickle'].Temps.max() - data[data.Period == 'Pickle'].Temps.min())/3600
    Low = IC_durrees["Pickle"][2]/3600
    Up = IC_durrees["Pickle"][3]/3600
    Lower = IC_durrees["Pickle"][0]/3600
    Upper = IC_durrees["Pickle"][1]/3600
    min = 0#round(0.8*Lower, 1)
    max = round(1.2*Upper, 1)
    color={"gradient":False,"ranges":{"rgba(228,56,35,1)":[min,Low],"rgba(242, 144, 24,1)": [Low,Lower],col_pickle:[Lower,Upper],"#f29018": [Upper,Up],"red":[Up,max]}}
    if duree < Low:
        text = "Pickle : Trop court"
        col = "rgba(228,56,35,1)"
    elif duree < Lower:
        text = "Pickle : Un peu trop court"
        col = "rgba(242, 144, 24,1)"
    elif duree > Up:
        text = "Pickle : Trop long"
        col = "rgba(228,56,35,1)"
    elif duree > Upper: 
        text = "Pickle : Un peu trop long"
        col = "rgba(242, 144, 24,1)"
    else :
        text = "Pickle : Dans l'intervalle de tollérance"
        col = "#84e61c"
    return [duree, color, min, max, text, col]

@app.callback(
    [Output('gauge_o2_fin_pickle', 'value'),
    Output('gauge_o2_fin_pickle', 'color'),
    Output('gauge_o2_fin_pickle', 'min'),
    Output('gauge_o2_fin_pickle', 'max'),
    Output('o2_Time_FinPickle', 'children'),
    Output('o2_Time_FinPickle_ind', 'color')],
    [Input('Lot', 'value'),
     Input("RawData_Tab2", "value")]
)
def update_gauge_o2_fin_pickle(lot_name, raw):
    data = Data_Foulon1[Data_Foulon1["NomLot"]==lot_name]
    duree = (data[data.Period == 'Fin_Pickle'].Temps.max() - data[data.Period == 'Fin_Pickle'].Temps.min())/3600
    Low = IC_durrees["Fin_Pickle"][2]/3600
    Up = IC_durrees["Fin_Pickle"][3]/3600
    Lower = IC_durrees["Fin_Pickle"][0]/3600
    Upper = IC_durrees["Fin_Pickle"][1]/3600
    min = 0#round(0.8*Lower, 1)
    max = round(1.2*Upper, 1)
    color={"gradient":False,"ranges":{"rgba(228,56,35,1)":[min,Low],"rgba(242, 144, 24,1)": [Low,Lower], col_fin_pickle:[Lower,Upper],"#f29018": [Upper,Up],"red":[Up,max]}}
    if duree < Low:
        text = "Fin de Pickle : Trop court"
        col = "rgba(228,56,35,1)"
    elif duree < Lower:
        text = "Fin de Pickle : Un peu trop court"
        col = "rgba(242, 144, 24,1)"
    elif duree > Up:
        text = "Fin de Pickle : Trop long"
        col = "rgba(228,56,35,1)"
    elif duree > Upper: 
        text = "Fin de Pickle : Un peu trop long"
        col = "rgba(242, 144, 24,1)"
    else :
        text = "Fin de Pickle : Dans l'intervalle de tollérance"
        col = "#84e61c"
    return [duree, color, min, max, text, col]

@app.callback(
    [Output('gauge_o2_pretannage', 'value'),
    Output('gauge_o2_pretannage', 'color'),
    Output('gauge_o2_pretannage', 'min'),
    Output('gauge_o2_pretannage', 'max'),
    Output('o2_Time_Pretannage', 'children'),
    Output('o2_Time_Pretannage_ind', 'color')],
    [Input('Lot', 'value'),
     Input("RawData_Tab2", "value")]
)
def update_gauge_o2_pretannage(lot_name, raw):
    data = Data_Foulon1[Data_Foulon1["NomLot"]==lot_name]
    duree = (data[data.Period == 'Tannage'].Temps.max() - data[data.Period == 'Pretannage'].Temps.min())/3600
    Low = IC_durrees["Pretannage_Tannage"][2]/3600
    Up = IC_durrees["Pretannage_Tannage"][3]/3600
    Lower = IC_durrees["Pretannage_Tannage"][0]/3600
    Upper = IC_durrees["Pretannage_Tannage"][1]/3600
    min = 0#round(0.8*Lower, 1)
    max = round(1.2*Upper, 1)
    color={"gradient":False,"ranges":{"rgba(228,56,35,1)":[min,Low], 
    "rgba(242, 144, 24,1)": [Low,Lower], 
    col_pretannage:[Lower,Upper],
    "#f29018": [Upper,Up], 
    "red":[Up,max]}}
    if duree < Low:
        text = "Prétannage : Trop court"
        col = "rgba(228,56,35,1)"
    elif duree < Lower:
        text = "Prétannage : Un peu trop court"
        col = "rgba(242, 144, 24,1)"
    elif duree > Up:
        text = "Prétannage : Trop long"
        col = "rgba(228,56,35,1)"
    elif duree > Upper: 
        text = "Prétannage : Un peu trop long"
        col = "rgba(242, 144, 24,1)"
    else :
        text = "Prétannage : Dans l'intervalle de tollérance"
        col = "#84e61c"
    return [duree, color, min, max, text, col]


@app.callback(
    [Output('Scatter_plot2', 'figure')],
    [Input('Lot', 'value'),
     Input("RawData_Tab2", "value"),
     Input('Scatter_plot3', 'relayoutData')]
)
def update_graph_Tab2_ScatterPlot2(lot_name, raw, select_data):
    data = Data_Foulon1[Data_Foulon1["NomLot"]==lot_name]
    Lot=data[(data.Ph_sd_curve.isnull()==False) & (data.Period != "autre")].reset_index(drop=True)
    Lot.loc[:,"Temps"]=np.arange(0,Lot.shape[0]*30,30)
    try:
        res = segDTW(cc,Lot)
        x=np.concatenate((res[1], 
                max(res[1])+res[2] + 1,
                max(res[1])+max(res[2])+res[3] + 1,
                max(res[1])+max(res[2])+max(res[3])+res[4] + 1))
    except ValueError:
        print("Ca marche pas, donc je fais sur tout le signal et il faudra l'écrire quelquepart")
        res = allDTW(cc,Lot)
        x = res[1]
    cc_recal=cc.loc[x]
    cc_recal.loc[:,"Temps"]=np.arange(0,cc_recal.shape[0]*30,30)

    figure1 = go.Figure()
    figure1.add_trace(go.Scatter(x=cc_recal.Temps/3600,
                y=cc_recal.lim_Ph_sup,
                fill=None,
                mode='lines',
                marker_color='#919191', 
                showlegend=False, 
                hovertemplate = '%{y:.2f}',
                name = "Limite Sup.")),
    figure1.add_trace(go.Scatter(x=cc_recal.Temps/3600,
                y=cc_recal.lim_Ph_inf,
                mode='lines',
                fill='tonexty',
                marker_color='#919191', 
                showlegend=False, 
                hovertemplate = '%{y:.2f}',
                name = "Limite Inf.")),
    figure1.add_trace(go.Scatter(x=Lot["Temps"]/3600,
                y=Lot["Ph_curve"],
                mode="lines",
                marker_color = 'rgba(26, 118, 189, 1)',
                showlegend=False,
                hovertemplate = '%{y:.2f}',
                name = "pH")),
    figure1.update_layout(go.Layout(
            yaxis = dict(title = "pH", 
                            rangemode = "nonnegative"),
            xaxis = dict(title = "Temps (Heures)",
                            rangemode = "nonnegative")
    )),
    if raw==["Raw"]:
        figure1.add_trace(go.Scatter(
            x=Lot["Temps"]/3600,
            y=Lot["Ph_inter"],
            mode='markers',
            marker_color='rgba(173, 173, 173, .5)', 
            hovertemplate = '%{y:.2f}',
            showlegend=False
            )
        )
    try:
        figure1['layout'] = {'xaxis':{'range':[select_data['xaxis.range[0]'],select_data['xaxis.range[1]']]},
    }
    except:
        figure1['layout'] = {'xaxis':{'range':[min(Lot["Temps"]/3600),max(Lot["Temps"]/3600)]}}
    figure1.update_xaxes(title_text='Temps (Heures)')
    figure1.update_yaxes(title_text='pH')
    figure1.update_layout(
        margin=dict(l=50, r=20, t=40, b=60),
        shapes=[
            dict(
                type="rect", xref="x", yref="paper",
                x0=0, y0=0,
                x1=cc_recal[cc_recal.Period=="Dechaulage"].Temps.max()/3600,
                y1=1,
                fillcolor=col_dechaulage,
                layer="below",
                line_width=0,
            ),
            dict(
                type="rect", xref="x", yref="paper",
                x0=cc_recal[cc_recal.Period=="Pickle"].Temps.min()/3600,
                y0=0,
                x1=cc_recal[cc_recal.Period=="Pickle"].Temps.max()/3600,
                y1=1,
                fillcolor=col_pickle,
                opacity=0.5,
                layer="below",
                line_width=0,
            ),
            dict(
                type="rect", xref="x", yref="paper",
                x0=cc_recal[cc_recal.Period=="Fin_Pickle"].Temps.min()/3600,
                y0=0,
                x1=cc_recal[cc_recal.Period=="Fin_Pickle"].Temps.max()/3600,
                y1=1,
                fillcolor=col_fin_pickle,
                layer="below",
                line_width=0,
            ),
            dict(
                type="rect", xref="x", yref="paper",
                x0=cc_recal[cc_recal.Period=="Pretannage"].Temps.min()/3600,
                y0=0,
                x1=cc_recal[cc_recal.Period=="Pretannage"].Temps.max()/3600,
                y1=1,
                fillcolor=col_pretannage,
                layer="below",
                line_width=0,
            ),
            dict(
                type="rect", xref="x", yref="paper",
                x0=cc_recal[cc_recal.Period=="Tannage"].Temps.min()/3600,
                y0=0,
                x1=cc_recal[cc_recal.Period=="Tannage"].Temps.max()/3600,
                y1=1,
                fillcolor=col_tannage,
                layer="below",
                line_width=0,
            )
        ]
    )     
    return [figure1]


@app.callback(
    [Output('Scatter_plot3', 'figure')],
    [Input('Lot', 'value'),
     Input("RawData_Tab2", "value"),
     Input('Scatter_plot2', 'relayoutData')],
)
def update_graph_Tab2_ScatterPlot3(lot_name, raw, select_data):
    data = Data_Foulon1[Data_Foulon1["NomLot"]==lot_name]
    Lot=data[(data.Ph_sd_curve.isnull()==False) & (data.Period != "autre")].reset_index(drop=True)
    Lot.loc[:,"Temps"]=np.arange(0,Lot.shape[0]*30,30)
    try:
        res = segDTW(cc,Lot)
        x=np.concatenate((res[1], 
                max(res[1])+res[2] + 1,
                max(res[1])+max(res[2])+res[3] + 1,
                max(res[1])+max(res[2])+max(res[3])+res[4] + 1))
    except ValueError:
        print("Ca marche pas, donc je fais sur tout le signal et il faudra l'écrire quelquepart")
        res = allDTW(cc,Lot)
        x = res[1]
    cc_recal=cc.loc[x]
    cc_recal.loc[:,"Temps"]=np.arange(0,cc_recal.shape[0]*30,30)

    figure = go.Figure()
    figure.add_trace(go.Scatter(x=cc_recal.Temps/3600,
                y=cc_recal.lim_Temp_sup,
                fill=None,
                mode='lines',
                marker_color='#919191', 
                showlegend=False, 
                hovertemplate = '%{y:.1f}',
                name = "Limite Sup.")),
    figure.add_trace(go.Scatter(x=cc_recal.Temps/3600,
                y=cc_recal.lim_Temp_inf,
                mode='lines',
                fill='tonexty',
                marker_color='#919191', 
                showlegend=False, 
                hovertemplate = '%{y:.1f}',
                name = "Limite Inf.")),
    figure.add_trace(go.Scatter(x=Lot["Temps"]/3600,
                y=Lot["Temp_curve"],
                mode="lines",
                marker_color = 'rgba(26, 118, 189, 1)',
                showlegend=False,
                hovertemplate = '%{y:.1f}',
                name = "pH")),
    figure.update_layout(go.Layout(
            yaxis = dict(title = "pH", 
                            rangemode = "nonnegative"),
            xaxis = dict(title = "Temps (Heures)",
                            rangemode = "nonnegative")
    )),
    if raw==["Raw"]:
        figure.add_trace(go.Scatter(
            x=Lot["Temps"]/3600,
            y=Lot["Temp_inter"],
            mode='markers',
            marker_color='rgba(173, 173, 173, .5)', 
            hovertemplate = '%{y:.1f}',
            showlegend=False
            )
        )
    try:
        figure['layout'] = {'xaxis':{'range':[select_data['xaxis.range[0]'],select_data['xaxis.range[1]']]},
    }
    except:
        figure['layout'] = {'xaxis':{'range':[min(Lot["Temps"]/3600),max(Lot["Temps"]/3600)]}}
    figure.update_xaxes(title_text='Temps (Heures)')
    figure.update_yaxes(title_text='Température (°C)')
    figure.update_layout(
        margin=dict(l=50, r=20, t=40, b=60),
        shapes=[
            dict(
                type="rect", xref="x", yref="paper",
                x0=0, y0=0,
                x1=cc_recal[cc_recal.Period=="Dechaulage"].Temps.max()/3600,
                y1=1,
                fillcolor=col_dechaulage,
                layer="below",
                line_width=0,
            ),
            dict(
                type="rect", xref="x", yref="paper",
                x0=cc_recal[cc_recal.Period=="Pickle"].Temps.min()/3600,
                y0=0,
                x1=cc_recal[cc_recal.Period=="Pickle"].Temps.max()/3600,
                y1=1,
                fillcolor=col_pickle,
                opacity=0.5,
                layer="below",
                line_width=0,
            ),
            dict(
                type="rect", xref="x", yref="paper",
                x0=cc_recal[cc_recal.Period=="Fin_Pickle"].Temps.min()/3600,
                y0=0,
                x1=cc_recal[cc_recal.Period=="Fin_Pickle"].Temps.max()/3600,
                y1=1,
                fillcolor=col_fin_pickle,
                layer="below",
                line_width=0,
            ),
            dict(
                type="rect", xref="x", yref="paper",
                x0=cc_recal[cc_recal.Period=="Pretannage"].Temps.min()/3600,
                y0=0,
                x1=cc_recal[cc_recal.Period=="Pretannage"].Temps.max()/3600,
                y1=1,
                fillcolor=col_pretannage,
                layer="below",
                line_width=0,
            ),
            dict(
                type="rect", xref="x", yref="paper",
                x0=cc_recal[cc_recal.Period=="Tannage"].Temps.min()/3600,
                y0=0,
                x1=cc_recal[cc_recal.Period=="Tannage"].Temps.max()/3600,
                y1=1,
                fillcolor=col_tannage,
                layer="below",
                line_width=0,
            )
        ]
    )
    return [figure]

@app.callback(
    [Output('Scatter_plot6', 'figure')],
    [Input('LotRef', 'value'),
     Input('LotTest', 'value'),
     Input('Scatter_plot7', 'relayoutData')]
)

def update_graph_o3_Plot_pH(LotRef, LotTest, select_data):
    data = Data_Foulon1[(Data_Foulon1.Ph_sd_curve.isnull() == False) & (Data_Foulon1.Period != "autre")].reset_index(drop=True)
    data_lot1 = data[data["NomLot"]==LotRef].reset_index(drop=True)
    data_lot2 = data[data["NomLot"]==LotTest].reset_index(drop=True)

    data_lot1.loc[:,"Temps"]=np.arange(0,data_lot1.shape[0]*30,30)
    data_lot2.loc[:,"Temps"]=np.arange(0,data_lot2.shape[0]*30,30)

    try:
        res = segDTW(data_lot2,data_lot1)
        x = np.concatenate((res[1],
                            max(res[1]) + res[2] + 1,
                            max(res[1]) + max(res[2]) + res[3] + 1,
                            max(res[1]) + max(res[2]) + max(res[3]) + res[4] + 1))
    except ValueError:
        print("Ca marche pas, donc je fais sur tout le signal et il faudra l'écrire quelquepart")
        res = allDTW(data_lot2,data_lot1)
        x = res[1]

    data_lot2_recal=data_lot2.loc[x]
    data_lot2_recal.loc[:, "Temps"] = np.arange(0,data_lot2_recal.shape[0]*30,30)

    figure = go.Figure()
    figure.add_trace(go.Scatter(
        x=data_lot1["Temps"]/3600,
        y=data_lot1["Ph_curve"],
        mode='lines',
        hovertemplate = '%{y:.2f}',
        name = "Lot Référence",
        marker_color='rgba(252, 17, 0, 1)', 
        showlegend=True)),
    figure.add_trace(go.Scatter(
        x=data_lot2_recal["Temps"]/3600,
        y=data_lot2_recal["Ph_curve"], 
        mode="lines",
        hovertemplate = '%{y:.2f}',
        name = "Lot Analysé",
        marker_color = 'rgba(26, 118, 189, 1)',
        showlegend=True)),
    figure.update_layout(go.Layout(
        yaxis = dict(title = "pH", rangemode = "nonnegative"),
        xaxis = dict(title = "Temps (Heures)", rangemode = "nonnegative"))),
    try:
        figure['layout'] = {'xaxis':{'range':[select_data['xaxis.range[0]'],select_data['xaxis.range[1]']]},
    }
    except:
        figure['layout'] = {'xaxis':{'range':[min(data_lot1["Temps"]/3600),max(data_lot1["Temps"]/3600)]}}
    figure.update_layout(
        margin=dict(l=50, r=20, t=40, b=60),
    shapes=[
            dict(
                type="rect", xref="x", yref="paper",
                x0=0, y0=0,
                x1=data_lot1[data_lot1.Period=="Dechaulage"].Temps.max()/3600,
                y1=1,
                fillcolor=col_dechaulage,
                layer="below",
                line_width=0,
            ),
            dict(
                type="rect", xref="x", yref="paper",
                x0=data_lot1[data_lot1.Period=="Pickle"].Temps.min()/3600,
                y0=0,
                x1=data_lot1[data_lot1.Period=="Pickle"].Temps.max()/3600,
                y1=1,
                fillcolor=col_pickle,
                opacity=0.5,
                layer="below",
                line_width=0,
            ),
            dict(
                type="rect", xref="x", yref="paper",
                x0=data_lot1[data_lot1.Period=="Fin_Pickle"].Temps.min()/3600,
                y0=0,
                x1=data_lot1[data_lot1.Period=="Fin_Pickle"].Temps.max()/3600,
                y1=1,
                fillcolor=col_fin_pickle,
                layer="below",
                line_width=0,
            ),
            dict(
                type="rect", xref="x", yref="paper",
                x0=data_lot1[data_lot1.Period=="Pretannage"].Temps.min()/3600,
                y0=0,
                x1=data_lot1[data_lot1.Period=="Pretannage"].Temps.max()/3600,
                y1=1,
                fillcolor=col_pretannage,
                layer="below",
                line_width=0,
            ),
            dict(
                type="rect", xref="x", yref="paper",
                x0=data_lot1[data_lot1.Period=="Tannage"].Temps.min()/3600,
                y0=0,
                x1=data_lot1[data_lot1.Period=="Tannage"].Temps.max()/3600,
                y1=1,
                fillcolor=col_tannage,
                layer="below",
                line_width=0,
            )
        ]),
    
    return [figure]


@app.callback(
    [Output('Scatter_plot7', 'figure')],
    [Input('LotRef', 'value'),
     Input('LotTest', 'value'),
     Input('Scatter_plot6', 'relayoutData')]
)

def update_graph_o3_Plot_Temp(LotRef, LotTest, select_data):
    data = Data_Foulon1[(Data_Foulon1.Ph_sd_curve.isnull() == False) & (Data_Foulon1.Period != "autre")].reset_index(drop=True)
    data_lot1 = data[data["NomLot"]==LotRef].reset_index(drop=True)
    data_lot2 = data[data["NomLot"]==LotTest].reset_index(drop=True)

    data_lot1.loc[:,"Temps"]=np.arange(0,data_lot1.shape[0]*30,30)
    data_lot2.loc[:,"Temps"]=np.arange(0,data_lot2.shape[0]*30,30)

    try:
        res = segDTW(data_lot2,data_lot1)
        x = np.concatenate((res[1],
                            max(res[1]) + res[2] + 1,
                            max(res[1]) + max(res[2]) + res[3] + 1,
                            max(res[1]) + max(res[2]) + max(res[3]) + res[4] + 1))
    except ValueError:
        print("Ca marche pas, donc je fais sur tout le signal et il faudra l'écrire quelquepart")
        res = allDTW(data_lot2,data_lot1)
        x = res[1]

    data_lot2_recal=data_lot2.loc[x]
    data_lot2_recal.loc[:, "Temps"] = np.arange(0,data_lot2_recal.shape[0]*30,30)

    figure = go.Figure()
    figure.add_trace(go.Scatter(
        x=data_lot1["Temps"]/3600,
        y=data_lot1["Temp_curve"],
        mode='lines',
        hovertemplate = '%{y:.1f}',
        marker_color='rgba(252, 17, 0, 1)', 
        name = "Lot Référence",
        showlegend=True)),
    figure.add_trace(go.Scatter(
        x=data_lot2_recal["Temps"]/3600,
        y=data_lot2_recal["Temp_curve"], 
        mode="lines",
        hovertemplate = '%{y:.1f}',
        marker_color = 'rgba(26, 118, 189, 1)',
        name = "Lot Analysé",
        showlegend=True)),
    figure.update_layout(go.Layout(
        yaxis = dict(title = "Température (°C)", rangemode = "nonnegative"),
        xaxis = dict(title = "Temps (Heures)", rangemode = "nonnegative"))),

    try:
        figure['layout'] = {'xaxis':{'range':[select_data['xaxis.range[0]'],select_data['xaxis.range[1]']]},
    }
    except:
        figure['layout'] = {'xaxis':{'range':[min(data_lot1["Temps"]/3600),max(data_lot1["Temps"]/3600)]}}
    figure.update_layout(
        margin=dict(l=50, r=20, t=40, b=60),
    shapes=[
            dict(
                type="rect", xref="x", yref="paper",
                x0=0, y0=0,
                x1=data_lot1[data_lot1.Period=="Dechaulage"].Temps.max()/3600,
                y1=1,
                fillcolor=col_dechaulage,
                layer="below",
                line_width=0,
            ),
            dict(
                type="rect", xref="x", yref="paper",
                x0=data_lot1[data_lot1.Period=="Pickle"].Temps.min()/3600,
                y0=0,
                x1=data_lot1[data_lot1.Period=="Pickle"].Temps.max()/3600,
                y1=1,
                fillcolor=col_pickle,
                opacity=0.5,
                layer="below",
                line_width=0,
            ),
            dict(
                type="rect", xref="x", yref="paper",
                x0=data_lot1[data_lot1.Period=="Fin_Pickle"].Temps.min()/3600,
                y0=0,
                x1=data_lot1[data_lot1.Period=="Fin_Pickle"].Temps.max()/3600,
                y1=1,
                fillcolor=col_fin_pickle,
                layer="below",
                line_width=0,
            ),
            dict(
                type="rect", xref="x", yref="paper",
                x0=data_lot1[data_lot1.Period=="Pretannage"].Temps.min()/3600,
                y0=0,
                x1=data_lot1[data_lot1.Period=="Pretannage"].Temps.max()/3600,
                y1=1,
                fillcolor=col_pretannage,
                layer="below",
                line_width=0,
            ),
            dict(
                type="rect", xref="x", yref="paper",
                x0=data_lot1[data_lot1.Period=="Tannage"].Temps.min()/3600,
                y0=0,
                x1=data_lot1[data_lot1.Period=="Tannage"].Temps.max()/3600,
                y1=1,
                fillcolor=col_tannage,
                layer="below",
                line_width=0,
            )
        ]),
    
    return [figure]


if __name__ == '__main__':
    app.run_server(debug=True, port=8080)
